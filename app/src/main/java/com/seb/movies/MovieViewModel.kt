package com.seb.movies

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.seb.movies.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MovieViewModel(private var movieApi: Api) : ViewModel(), MovieViewModelInterface {

    private val movieLiveData = SingleLiveEvent<Movie>()
    private val errorLiveData = SingleLiveEvent<String>()

    fun movieDetails(): LiveData<Movie> = movieLiveData
    fun error(): LiveData<String> = errorLiveData

    private var movieApi: Api = Api.get()

    private val parametersMap: MutableMap<String, String> = mutableMapOf("apikey" to Constants.API_KEY)

    override fun findMovie() {
        callMovieApi(parametersMap)
    }

    override fun setMovieTitle(title: String) {
        parametersMap["t"] = title
    }

    override fun setMovieYear(year: String) {
        parametersMap["y"] = year
    }

    override fun setMovieType(type: String) {
        parametersMap["type"] = type
    }

    override fun setPlotType(plotType: String) {
        if (plotType != "full") {
            parametersMap.remove("plot")
        } else {
            parametersMap["plot"] = plotType
        }
    }

    private fun callMovieApi(parameters: Map<String, String>) {
        val filteredParams = parameters.filter { it.value.isNotEmpty() }
        movieApi.getMovie(filteredParams).enqueue(object : Callback<Movie> {
            override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                if (response.isSuccessful) {
                    response.body()?.let(movieLiveData::postValue)
                } else {
                    errorLiveData.postValue("${response.code()}")
                }
            }

            override fun onFailure(call: Call<Movie>, t: Throwable) {
                errorLiveData.postValue(t.message ?: "Nieznany błąd")
            }
        })
    }
}
