package com.seb.movies

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.seb.movies.utils.EditTextWatcher
import com.seb.movies.utils.SpinnerSelectedItemListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val viewModel = ViewModelProviders.of(this).get(MovieViewModel(Api.get())::class.java)
        setupListeners(viewModel)
        viewModel.movieDetails().observe(this, Observer(::showMovieDetailsActivity))
        viewModel.error().observe(this, Observer(::showError))
    }

    private fun setupListeners(viewModel: MovieViewModel) {
        movieTitle.addTextChangedListener(EditTextWatcher(viewModel::setMovieTitle))
        movieYear.addTextChangedListener(EditTextWatcher(viewModel::setMovieYear))
        movieType.onItemSelectedListener = SpinnerSelectedItemListener(viewModel::setMovieType)
        plotType.onItemSelectedListener = SpinnerSelectedItemListener(viewModel::setPlotType)
        findMovie.setOnClickListener { viewModel.findMovie() }
    }


    private fun showMovieDetailsActivity(movie: Movie?) {
        if (movie == null) return
        val intent = Intent(this, MovieDetailsActivity::class.java)
        intent.putExtra(Constants.MOVIE_INTENT_KEY, movie)
        startActivity(intent)
    }


    private fun showError(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

}
