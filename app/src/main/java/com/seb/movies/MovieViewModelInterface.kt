package com.seb.movies

interface MovieViewModelInterface {
    fun findMovie()
    fun setMovieTitle(title: String)
    fun setMovieYear(year: String)
    fun setMovieType(type: String)
    fun setPlotType(plotType: String)
}