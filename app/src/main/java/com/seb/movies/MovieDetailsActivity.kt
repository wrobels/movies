package com.seb.movies

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_movie_details.*

class MovieDetailsActivity : AppCompatActivity() {
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)

        val foundMovie = intent.getParcelableExtra(Constants.MOVIE_INTENT_KEY) as Movie
        setMovieInfos(foundMovie)
    }

    private fun setMovieInfos(foundMovie: Movie) {
        movieTitleTextView.text = foundMovie.title
        movieYearTextView.text = foundMovie.year
        moviePlotTextView.text = foundMovie.plot
        movieGenreTextView.text = foundMovie.genre
        movieTypeTextView.text = foundMovie.type
        movieCountryTextView.text = foundMovie.country
        movieLanguageTextView.text = foundMovie.language
        movieDirectorTextView.text = foundMovie.director
    }
}
