package com.seb.movies.exception

import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.internal.Util.UTF_8
import org.json.JSONObject

object ExceptionInterceptor : Interceptor {
    private const val ERROR_KEY = "Error"

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())
        val source = response.body()?.source() ?: return response
        source.request(Long.MAX_VALUE)
        val content = source.buffer().clone().readString(UTF_8)
        val jsonObject = JSONObject(content)
        val errorMessage = jsonObject.optString(ERROR_KEY)
        if (errorMessage.isNotEmpty()) {
            throw MovieException(errorMessage)
        } else {
            return response
        }
    }
}
