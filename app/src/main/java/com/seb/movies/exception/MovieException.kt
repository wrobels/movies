package com.seb.movies.exception

import java.io.IOException

class MovieException(override val message: String? = null) : IOException()
