package com.seb.movies.utils

import android.text.Editable
import android.text.TextWatcher

class EditTextWatcher(private val listener: (String) -> Unit) : TextWatcher {

    override fun afterTextChanged(s: Editable?) = Unit
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        s?.let { listener(it.toString()) }
    }
}
