package com.seb.movies.utils

import android.view.View
import android.widget.AdapterView

class SpinnerSelectedItemListener(private val listener: (String) -> Unit) : AdapterView.OnItemSelectedListener {

    override fun onNothingSelected(parent: AdapterView<*>?) = Unit

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        parent?.let { listener(it.adapter.getItem(position) as String) }
    }
}
