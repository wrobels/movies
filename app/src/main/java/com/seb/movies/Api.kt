package com.seb.movies

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface Api {

    @GET(".")
    fun getMovie(@QueryMap parameters: Map<String, String>): Call<Movie>

    companion object {
        fun get(): Api = RetrofitProvider.retrofit.create(Api::class.java)
    }
}
