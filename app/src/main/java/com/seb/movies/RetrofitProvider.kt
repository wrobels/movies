package com.seb.movies

import com.seb.movies.exception.ExceptionInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitProvider {
    private const val BASE_URL = "http://www.omdbapi.com/"

    private val client: OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(ExceptionInterceptor)
        .build()

    val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()

}
