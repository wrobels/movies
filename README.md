# Movies

Application allows user to check informations about movies.



User enters movie title, year and chooses movie type and plot length.
After clicking 'Find Movie' button, new Activity starts and movie details are displayed.

#### Libraries & Patterns

Application based on MVVM pattern.
Retrofit library has been used to obtain data from OMDb API.

#### Screenshots

##### Screen 1

<img src="./screens/scr-1.jpg" width="150">

##### Screen 2

<img src="./screens/scr-2.jpg" width="150">

